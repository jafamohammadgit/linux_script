#!/usr/bin/bash
sudo apt-get update
sudo apt-get upgrade
sudo apt install zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
sudo apt-get install snapd
snap install micro --classic
echo 'you must replace and adding:'
echo '--------------------------------------'
echo ' ZSH_THEME="powerlevel10k/powerlevel10k'
echo 'POWERLEVEL9K_MODE="nerdfont-complete"'
echo 'ENABLE_CORRECTION="true"'
echo 'plugins=(git zsh-autosuggestions zsh-syntax-highlighting)'
echo 'ZSH_DISABLE_COMPFIX=true'
echo 'export ZSH="/Users/shivam/.oh-my-zsh"'
echo '--------------------------------------'
echo "CLICK ENTER: "
read
micro ~/.zshrc
sudo apt-get install p10k
sudo apt-get install build-essential
sudo apt install git -y
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
brew doctor
brew install gcc
brew install zsh
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
sudo apt-get install gem
sudo gem install colorls
sudo apt install ruby-full
brew install exa
curl -fsSL https://cli.secman.dev | bash
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
echo '----------------------------------------------'
echo "Add this lines to zsh in home and in root: "
echo """
if [ -x "$(command -v colorls)" ]; then
    alias ls="colorls"
    alias la="colorls -al"
fi

if [ -x "$(command -v exa)" ]; then
    alias ls="exa"
    alias la="exa --long --all --group"
fi

"""
echo "and in up : add This line: it not working"
echo  "source ~/.zshrc"
echo '---------------------------------------------'
echo "Enter for configure: "
p10k configure