from datetime import datetime
from colorama import Fore
G = Fore.GREEN
C = Fore.CYAN
RT = Fore.RESET
Y = Fore.YELLOW

def log_time(func):
    """
        show install time any app
    """
    def get_func(*args, **kwargs):
        start = datetime.now()
        func(*args, **kwargs)
        out = datetime.now()
        find = out - start
        print(f"\n\t{G}Time Out{Y}: {C}{find}{RT}\n")  # show time out - time install a app
    return get_func